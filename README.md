# Webpack Boilerplate

[![MIT License][license-image]][license-url]

## Overview

## Dependencies

1. Node.js >= 12.3.1
2. NPM >= 6.9.0

[license-url]: LICENSE
[license-image]: http://img.shields.io/badge/license-MIT-000000.svg?style=flat-square
