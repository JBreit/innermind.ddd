import 'module-alias/register';
import { container } from './container';

const application: any = container.resolve('application');

application.start()
  .catch(error => {
    application.logger.error(error.stack);

    process.exit();
  });
