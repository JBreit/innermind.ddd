import * as t from 'tcomb';

export const Token = t.struct({
  email: t.String,
  password: t.String
});
