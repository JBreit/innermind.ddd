import * as t from 'tcomb';
import { compose } from 'ramda';
import { cleanData } from '@src/domain/helper';

export const User = compose(
  cleanData,
  t.struct({
    id: t.maybe(t.String),
    firstName: t.String,
    lastName: t.String,
    middleName: t.String,
    email: t.String,
    password: t.maybe(t.String),
    roleId: t.Number,
    verificationCode: t.maybe(t.String),
    isVerified: t.maybe(t.Number),
    isDeleted: t.Number,
    createdBy: t.maybe(t.String),
    updatedBy: t.maybe(t.String),
    createdAt: t.maybe(t.Date),
    updatedAt: t.maybe(t.Date)
  })
);
