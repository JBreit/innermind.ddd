import {
  complement,
  compose,
  isNil,
  pickBy
} from 'ramda';

const notNull = compose(complement(isNil));

export const cleanData = entity => pickBy(notNull, entity);
