import { createContainer, asValue, asFunction } from 'awilix';
import { application } from './application';
import { authentication, router, server } from './interfaces/http';
import database from './infrastructure/database';
import logger from './infrastructure/logging/logger';
import { repository } from './infrastructure/repositories';
import { JSONWebToken } from './infrastructure/jwt';
import { response } from './infrastructure/support/response';
import { date } from './infrastructure/support/date';
import { config } from './config';

export const container = createContainer()
  .register({
    application: asFunction(application).singleton(),
    authentication: asFunction(authentication).singleton(),
    config: asValue(config),
    database: asFunction(database).singleton(),
    date: asFunction(date).singleton(),
    logger: asFunction(logger).singleton(),
    jwt: asFunction(JSONWebToken).singleton(),
    repository: asFunction(repository).singleton(),
    response: asFunction(response).singleton(),
    router: asFunction(router).singleton(),
    server: asFunction(server).singleton()
  });
