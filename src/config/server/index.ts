export default {
  development: {
    host: process.env.HOST,
    port: process.env.PORT
  },
  production: {
    host: process.env.HOST,
    port: process.env.PORT
  },
  staging: {
    host: process.env.HOST,
    port: process.env.PORT
  },
  test: {
    host: process.env.HOST,
    port: process.env.PORT
  }
};
