import { existsSync } from 'fs';
import { join, resolve } from 'path';
import * as dotenv from 'dotenv';

const envFound = dotenv.config({ path: resolve('.env') });

if (!envFound) {
  throw new Error('Couldn\'t find .env file.');
}

// eslint-disable-next-line global-require
const loadEnvConfig = env => require(join(__dirname, 'environments', env)); // eslint-disable-line import/no-dynamic-require

const loadDbConfig = env => {
  if (existsSync(join(__dirname, './database'))) {
    // eslint-disable-next-line global-require
    return require('./database').default[env];
  }
  throw new Error('Database Configuration Required');
};

const loadServerConfig = env => {
  if (existsSync(join(__dirname, './server'))) {
    // eslint-disable-next-line global-require
    return require('./server').default[env];
  }
  throw new Error('Server Configuration Required');
};

const ENV = process.env.NODE_ENV || 'development';
const envConfig = loadEnvConfig(ENV);
const dbConfig = loadDbConfig(ENV);
const serverConfig = loadServerConfig(ENV);

export const config = {
  env: ENV,
  db: dbConfig,
  server: serverConfig,
  ...envConfig
};
