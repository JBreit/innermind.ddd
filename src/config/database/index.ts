export default {
  development: {
    url: `postgres://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_URL}:5432/${process.env.DB_NAME}`,
    dialect: 'postgres'
  },
  production: {
    url: `postgres://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_URL_PRODUCTION}:5432/${process.env.DB_NAME}`,
    dialect: 'postgres',
    ssl: true,
    dialectOptions: {
      ssl: {
        require: true
      }
    }
  },
  staging: {
    url: `postgres://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_URL_STAGING}:5432/${process.env.DB_NAME}`,
    dialect: 'postgres',
    ssl: true,
    dialectOptions: {
      ssl: {
        require: true
      }
    }
  },
  test: {
    url: `postgres://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_URL_TEST}:5432/${process.env.DB_NAME}`,
    dialect: 'postgres',
    logging: false
  }
};
