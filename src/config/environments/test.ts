export const test = {
  version: process.env.APP_VERSION,
  host: process.env.HOST || '127.0.0.1',
  port: process.env.PORT || 8080,
  timezone: process.env.TIMEZONE,
  logging: {
    maxsize: 100 * 1024,
    maxFiles: 2,
    colorize: true
  },
  authSecret: process.env.SECRET,
  authSession: {
    session: false
  }
};
