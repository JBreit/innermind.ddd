import { comparePassword } from '@src/infrastructure/encryption';
import { toEntity } from './transform';

export const User = ({ model }) => {
  const create = (...args) => model.create(...args)
    .then(({ dataValues }) => toEntity(dataValues));

  const getAll = (...args) => model.findAll(...args)
    .then(entity => entity.map(data => toEntity(data.dataValues)));

  const update = (...args) => model.update(...args)
    .catch(error => {
      throw new Error(error);
    });

  const destroy = (...args) => model.destroy(...args);

  const findById = (...args) => model.findByPk(...args)
    .then(({ dataValues }) => toEntity(dataValues))
    .catch(err => { throw new Error(err); });

  const findOne = (...args) => model.findOne(...args)
    .then(({ dataValues }) => toEntity(dataValues))
    .catch(err => { throw new Error(err); });

  const validate = endcodedPassword => password => comparePassword(password, endcodedPassword);

  return {
    create,
    destroy,
    findById,
    findOne,
    getAll,
    update,
    validatePassword: validate
  };
};
