import { User } from './user';

export const repository = ({ database }) => {
  const userModel = database.models.users;

  return {
    userRepository: User({ model: userModel })
  };
};
