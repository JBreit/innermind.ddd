import { database } from '@src/infrastructure/sequelize';

export default ({ logger, config }) => {
  if (!config.db) {
    logger.log({ level: 'info', message: 'Database config file log not found, disabling database.' });

    return false;
  }

  return database({ config, basePath: __dirname });
};
