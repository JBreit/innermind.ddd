import { assoc } from 'ramda';

export const response = ({ config }) => {
  const defaultResponse = (success = true) => ({
    success,
    version: config[config.env].version,
    date: new Date()
  });

  const Success = data => assoc('data', data, defaultResponse(true));
  const Fail = data => assoc('error', data, defaultResponse(false));

  return { Success, Fail };
};
