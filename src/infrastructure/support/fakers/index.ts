import dotenv from 'dotenv';
import { resolve } from 'path';

dotenv.config();

export default async fixtureURI => {
  const basePath = process.env.NODE_ENV || 'development';
  const fixturePath = await resolve(`src/infrastructure/support/fakers/${basePath}`, fixtureURI);
  const Fixture = await import(fixturePath);

  return Fixture.default();
};
