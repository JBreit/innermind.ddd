import * as moment from 'moment-timezone';

export const date = ({ config }) => {
  const currentDate = moment.tz.setDefault(config.timezone);
  const addHour = duration => currentDate.add(duration, 'hours');

  return { addHour };
};
