import { existsSync, mkdirSync } from 'fs';
import * as winston from 'winston';

if (!existsSync('logs')) {
  mkdirSync('logs');
}

export default ({ config }) => {
  const {
    colorize, combine, json, simple
  } = winston.format;
  const { File } = winston.transports;
  const { env } = config;
  const defaults = { filename: `logs/${env}.log` };

  const envLogConfig = new File(Object.assign(config[env].logging, defaults));

  const errorLogConfig = new File({
    filename: 'logs/error.log',
    level: 'error'
  });

  const combinedLogConfig = new File({
    filename: 'logs/combined.log',
    format: combine(simple())
  });

  // eslint-disable-next-line new-cap
  const logger = winston.createLogger({
    level: 'info',
    exceptionHandlers: [
      new File({ filename: 'logs/exceptions.log' })
    ],
    exitOnError: false,
    format: combine(colorize(), json()),
    transports: [envLogConfig, combinedLogConfig, errorLogConfig]
  });

  if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({ format: simple() }));
  }

  return { logger };
};
