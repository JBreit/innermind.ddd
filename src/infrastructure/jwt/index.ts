import jwt from 'jsonwebtoken';
import {
  compose, trim, replace, partialRight
} from 'ramda';

export const JSONWebToken = ({ config }) => ({
  singin: options => payload => jwt.sign(payload, config.authSecret, { ...options, expiresIn: '1h' }),
  verify: options => token => jwt.verify(token, config.authSecret, { ...options }),
  decode: options => token => {
    const opt = { ...options };
    const decodeToken = compose(partialRight(jwt.decode, [opt]), trim, replace(/JWT|jwt/g, ''));

    return decodeToken(token);
  }
});
