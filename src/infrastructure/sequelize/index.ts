import { readdirSync } from 'fs';
import { join } from 'path';
import { Sequelize } from 'sequelize';

export const database = ({ config, basePath }) => {
  const sequelize = new Sequelize(config.db.url, { ...config.db });

  const db = {
    sequelize,
    Sequelize,
    models: {}
  };

  const dir = join(basePath, './models');

  readdirSync(dir).forEach(file => {
    const modelDir = join(dir, file);
    const model = sequelize.import(modelDir);

    db.models[model.name] = model;
  });

  Object.keys(db.models).forEach(key => {
    if ('associate' in db.models[key]) {
      db.models[key].associate(db.models);
    }
  });

  return db;
};
