import Faker from '@src/infrastructure/support/fakers';

export default {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert('users', Faker('users'), {}),
  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('users', null, {})
};
