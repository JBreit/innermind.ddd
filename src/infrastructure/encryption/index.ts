import { compareSync, genSaltSync, hashSync } from 'bcrypt';

const salt = genSaltSync();

export const encryptPassword = password => hashSync(password, salt);
export const comparePassword = (password, encoded) => compareSync(password, encoded);
