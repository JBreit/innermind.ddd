import { container } from '@src/container';
// import { instance } from './instance';
import { router } from './router';

export default () => {
  const { logger, response: { Success, Fail } } = container.cradle;
  const app = {};

  return {
    app,
    router: router({ logger, response: { Success, Fail }, ...app })
  };
};
