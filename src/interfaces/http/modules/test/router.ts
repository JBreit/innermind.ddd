import { OK } from 'http-status';
import { Router } from 'express';

export const router = ({ logger, response: { Success, Fail } }) => {
  const api = Router();

  api.get('/', (req, res) => {
    res.status(OK).json(Success({ message: 'It Works Finally' }));
  });

  api.get('/test', (req, res) => {
    res.status(OK).json(Success({ message: 'Testing...' }));
  });

  return api;
};

export default router;
