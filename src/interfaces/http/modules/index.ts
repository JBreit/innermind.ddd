import swaggerJSDoc from 'swagger-jsdoc';
import { OK } from 'http-status';
import { Router } from 'express';

export const router = () => {
  const api = Router();

  const swaggerDefinition = {
    info: {
      title: 'Node DDD API Explorer',
      version: '1.0.0',
      description: 'Available REST Endpoints of Node DDD RESTful API'
    },
    host: `${process.env.API_SWAGGER}:${process.env.PORT}/api/${process.env.APP_VERSION}`,
    basePath: '/',
    securityDefinitions: {
      JWT: {
        description: '',
        type: 'apiKey',
        name: 'Authorization',
        in: 'header'
      }
    }
  };

  const options = {
    // import swaggerDefinitions
    swaggerDefinition,
    // path to the API docs
    apis: ['src/interfaces/http/modules/**/*.ts']
  };

  // initialize swagger-jsdoc
  const swaggerSpec = swaggerJSDoc(options);
  /**
   * @swagger
   * responses:
   *   Unauthorized:
   *     description: Unauthorized
   *   BadRequest:
   *     description: BadRequest / Invalid Input
   */

  /**
   * @swagger
   * /:
   *   get:
   *     tags:
   *       - Status
   *     description: Returns API status
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: API Status
   */
  api.get('/', (req, res) => {
    res.status(OK).json({ status: 'API working' });
  });

  api.get('/swagger.json', (req, res) => {
    res.status(OK).json(swaggerSpec);
  });

  return { router: api };
};

export default router;
