import { BAD_REQUEST, OK } from 'http-status';
import { Router } from 'express';

export const router = ({
  postUseCase,
  logger,
  response: { Success, Fail }
}) => {
  const api = Router();

  /**
 * @swagger
 * definitions:
 *   auth:
 *     properties:
 *       email:
 *         type: string
 *       password:
 *         type: string
 */

  /**
 * @swagger
 * /token:
 *   post:
 *     tags:
 *       - Authentication
 *     description: Authenticate
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         description: User's credentials
 *         in: body
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/auth'
 *     responses:
 *       200:
 *         description: Successfully login
 *       400:
 *         $ref: '#/responses/BadRequest'
 */
  api
    .post('/', (req, res) => {
      postUseCase
        .validate({ body: req.body })
        .then(data => {
          res.status(OK).json(Success(data));
        })
        .catch(err => {
          logger.logger.error(err);

          res.status(BAD_REQUEST).json(Fail(err.message));
        });
    });

  return api;
};

export default router;
