import { container } from '@src/container';
import { post } from '@src/application/token';

export const instance = () => {
  const { repository: { userRepository }, jwt } = container.cradle;
  const postUseCase = post({ userRepository, webToken: jwt });

  return { postUseCase };
};
