import { BAD_REQUEST, OK } from 'http-status';
import { Router } from 'express';

export const router = ({
  getUseCase,
  postUseCase,
  putUseCase,
  deleteUseCase,
  logger,
  authentication,
  response: { Success, Fail }
}) => {
  const api = Router();

  api.use(authentication.authenticate());

  api
    .get('/', (req, res) => {
      getUseCase
        .all(req, res)
        .then(data => {
          res.status(OK).json(Success(data));
        })
        .catch(error => {
          logger.logger.log({ level: 'error', message: error.message });

          res.status(BAD_REQUEST).json(Fail(error.message));
        });
    });

  api
    .post('/', (req, res) => {
      postUseCase
        .create({ body: req.body })
        .then(data => {
          res.status(OK).json(Success(data));
        })
        .catch(error => {
          logger.logger.log({ level: 'error', message: error.message }); // we still need to log every error for debugging

          res.status(BAD_REQUEST).json(Fail(error.message));
        });
    });

  api
    .put('/:id', (req, res) => {
      putUseCase
        .update({ id: req.params.id, body: req.body })
        .then(data => {
          res.status(OK).json(Success(data));
        })
        .catch(error => {
          logger.logger.log({ level: 'error', message: error.message }); // we still need to log every error for debugging

          res.status(BAD_REQUEST).json(Fail(error.message));
        });
    });

  api
    .delete('/:id', (req, res) => {
      deleteUseCase
        .remove({ id: req.params.id })
        .then(data => {
          res.status(OK).json(Success(data));
        })
        .catch(error => {
          logger.logger.log({ level: 'error', message: error.message }); // we still need to log every error for debugging

          res.status(BAD_REQUEST).json(Fail(error.message));
        });
    });

  return api;
};

export default router;
