import { container } from '@src/container';
import { router } from './router';
import { instance } from './instance';

export default () => {
  const { logger, response: { Success, Fail }, authentication } = container.cradle;
  const app = instance();
  logger.logger.log({ level: 'info', message: 'User Module Initialized' });
  return {
    app,
    router: router({
      logger, authentication, response: { Success, Fail }, ...app
    })
  };
};
