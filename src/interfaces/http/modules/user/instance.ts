import { container } from '@src/container';
import {
  get,
  post,
  put,
  remove
} from '@src/application/user';

export const instance = () => {
  const { repository: { userRepository } } = container.cradle;

  const getUseCase = get({ userRepository });
  const postUseCase = post({ userRepository });
  const putUseCase = put({ userRepository });
  const deleteUseCase = remove({ userRepository });

  return {
    getUseCase,
    postUseCase,
    putUseCase,
    deleteUseCase
  };
};
