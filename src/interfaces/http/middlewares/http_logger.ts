import morgan from 'morgan';

export default ({ logger }) => morgan('common', {
  stream: {
    write: message => {
      logger.log({ level: 'info', message: message.slice(0, -1) });
    }
  }
});
