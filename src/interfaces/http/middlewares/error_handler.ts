import { INTERNAL_SERVER_ERROR } from 'http-status';

export default (
  err,
  req,
  res,
  next,
  logger,
  config
) => {
  // eslint-disable-line no-unused-vars
  logger.logger.log({ level: 'error', message: err.message });

  const response = { type: 'InternalServerError', ...config.env === 'development' && { message: err.message, stack: err.stack } };

  res.status(INTERNAL_SERVER_ERROR).json(response);
};
