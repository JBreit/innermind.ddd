import passport from 'passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

export const authentication = ({ config, repository: { userRepository } }) => {
  const params = {
    secretOrKey: process.env.SECRET,
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt')
  };

  const strategy = new Strategy(params, (payload, done) => {
    userRepository.findById(payload.id)
      .then(user => {
        done(null, user);
      })
      .catch(error => done(error, null));
  });

  passport.use(strategy);

  passport.serializeUser((user, done) => {
    done(null, user);
  });

  passport.deserializeUser((user, done) => {
    done(null, user);
  });

  return {
    initialize: () => passport.initialize(),
    authenticate: () => passport.authenticate('jwt')
  };
};
