import statusMonitor from 'express-status-monitor';
import cors from 'cors';
import path from 'path';
import { json } from 'body-parser';
import compression from 'compression';

import { Router } from 'express';
import { partialRight } from 'ramda';

// import controller from '@src/interfaces/http/utilities/create_controller';
// import { createControllerRoutes } from '@src/interfaces/http/utilities/createControllerRoutes';
import httpLogger from '@src/interfaces/http/middlewares/http_logger';
import errorHandler from '@src/interfaces/http/middlewares/error_handler';

export default ({ config, logger, database }) => {
  const router = Router();

  /* istanbul ignore if */
  if (config.env === 'development') {
    router.use(statusMonitor());
  }

  /* istanbul ignore if */
  if (config.env !== 'test') {
    router.use(httpLogger(logger));
  }

  const apiRouter = Router();

  apiRouter
    .use(cors({
      origin: [
        'http://10.0.0.133:8080'
      ],
      methods: ['GET', 'POST', 'PUT', 'DELETE'],
      allowedHeaders: ['Content-Type', 'Authorization']
    }))
    .use(json())
    .use(compression());

  const createControllerRoutes = uri => {
    const controllerPath = path.resolve('src/interfaces/http/modules/', uri);
    // eslint-disable-next-line global-require
    const ctlr = require(controllerPath); // eslint-disable-line import/no-dynamic-require
    // console.log(ctlr.default().router);

    return ctlr.default();
  };

  /**
   * Add your API routes here
   *
   * You can use the `controllers` helper like this:
   * apiRouter.use('/users', controller(controllerPath))
   *
   * The `controllerPath` is relative to the `interfaces/http` folder
   */
  apiRouter.use('/', createControllerRoutes('index').router);
  apiRouter.use('/test', createControllerRoutes('test').router);
  // apiRouter.use('/token', createControllerRoutes('token').router);
  // apiRouter.use('/users', controller('user').router);
  // apiRouter.use('/companies', controller('company').router);

  router.use(`/api/${config[config.env].version}`, apiRouter);

  router.use(partialRight(errorHandler, [logger, config]));
  console.log(router);
  console.log(apiRouter);
  return router;
};
