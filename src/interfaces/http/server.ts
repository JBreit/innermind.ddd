// import { Promise } from 'es6-promise';
import { createServer } from 'http';
import { json, urlencoded } from 'body-parser';
import path from 'path';
import cookieParser from 'cookie-parser';
import express from 'express';
import favicon from 'serve-favicon';
import methodOverride from 'method-override';
import session from 'express-session';
import { normalizePort } from './utilities/normalizePort';

export default ({
  config,
  router,
  logger,
  authentication
}) => {
  const { env } = config;
  const { authSecret } = config[env];
  const { initialize } = authentication;
  const app = express();

  app
    .enable('verbose errors')
    .disable('x-powered-by')
    .set('json spaces', 2)
    .use(initialize())
    .use(router)
    .use(favicon(path.join(path.resolve('public'), 'assets', 'img', 'favicon.ico')))
    .use(urlencoded({ extended: true }))
    .use(json())
    .use(cookieParser())
    .use(methodOverride('X-HTTP-Override'))
    .use(session({
      secret: authSecret,
      resave: true,
      saveUninitialized: true,
      cookie: {
        maxAge: 60 * 60 * 1000
      }
    }))
    .use(express.static(path.resolve('public')));
  // .use((req, res, next) => {
  //   res.locals.csrftoken = req.session.csrfToken();

  //   return next();
  // });

  const server = createServer(app);
  const { host } = config.server;
  const port = normalizePort(config.server.port);

  const onListening = () => {
    const addr = server.address();
    const bind = typeof addr === 'string' ? `pipe: ${addr}` : `port: ${port}`;
    const message = `Server listening on ${bind}\n`;

    logger.logger.log({ level: 'info', message });
  };

  const onError = err => {
    if (err.syscall !== 'listen') {
      throw err;
    }

    const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

    switch (err.code) {
      case 'EACCES':
        logger.logger.log({ level: 'error', message: `${bind} requires elevated privileges` });

        process.stderr.write(`${bind} requires elevated privileges`);
        process.exit(1);

        break;
      case 'EADDRINUSE':
        logger.logger.log({ level: 'error', message: `${bind} is already in use` });

        process.stderr.write(`${bind} is already in use`);
        process.exit(1);

        break;
      default:
        throw err;
    }
  };

  return {
    app,
    start: () => new Promise((resolve, reject) => {
      server.on('listening', onListening);
      server.on('error', onError);
      server.listen(port, host);

      resolve();
    })
  };
};
