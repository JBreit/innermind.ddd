import path from 'path';

export default uri => {
  const controllerPath = path.resolve('src/interfaces/http/modules', uri);
  // eslint-disable-next-line global-require
  const controller = require(controllerPath); // eslint-disable-line import/no-dynamic-require

  return controller.default();
};
