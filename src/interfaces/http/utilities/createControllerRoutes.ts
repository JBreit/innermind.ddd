import path from 'path';

export const createControllerRoutes = uri => {
  const controllerPath = path.resolve('src/interfaces/http/modules/', uri);
  // eslint-disable-next-line global-require
  const ctlr = require(controllerPath); // eslint-disable-line import/no-dynamic-require

  return ctlr.default();
};

export default createControllerRoutes;
