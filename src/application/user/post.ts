import { User } from '@src/domain/entities/user';

export const post = ({ userRepository }) => {
  const create = ({ body }) => Promise
    .resolve()
    .then(() => {
      const password = body.password || 'test';
      const entity = { ...body, password };
      const user = User(entity);

      return userRepository.create(user);
    })
    .catch(error => {
      throw new Error(error);
    });

  return { create };
};
