import { User } from '@src/domain/entities/user';

export const put = ({ userRepository }) => {
  const update = ({ id, body }) => new Promise((resolve, reject) => {
    try {
      const user = User(body);

      userRepository.update(user, { where: { id } });

      resolve(user);
    } catch (error) {
      reject(error);
    }
  });

  return { update };
};
