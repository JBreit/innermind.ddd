export const get = ({ userRepository }) => {
  const all = () => Promise
    .resolve()
    .then(() => userRepository
      .getAll({
        attributes: [
          'id',
          'firstName',
          'lastName',
          'middleName',
          'email',
          'roleId',
          'isDeleted',
          'createdBy',
          'updateBy'
        ]
      }))
    .catch(error => {
      throw new Error(error);
    });

  return { all };
};
