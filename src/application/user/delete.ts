export const remove = ({ userRepository }) => ({ id }) => Promise
  .resolve()
  .then(() => userRepository.update({ isDeleted: 1 }, { where: { id } }))
  .catch(error => {
    throw new Error(error);
  });
