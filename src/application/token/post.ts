import { Token } from '@src/domain/entities/token';

export const post = ({ userRepository, webToken }) => {
  const validate = ({ body }) => new Promise((resolve, reject) => {
    try {
      const credentials: any = Token(body);
      const userCredentials = userRepository.findOne({
        attributes: [
          'id',
          'firstName',
          'lastName',
          'middleName',
          'email',
          'password',
          'roleId',
          'isDeleted',
          'createdBy'
        ],
        where: {
          email: credentials.mail,
          isDeleted: 0
        }
      });

      const validatePass = userRepository.validatePassword(userCredentials.password);

      if (!validatePass(credentials.password)) {
        throw new Error('Invalid Credentials');
      }

      const signIn = webToken.signin();

      resolve({
        token: signIn({
          id: userCredentials.id,
          firstName: userCredentials.firstName,
          lastName: userCredentials.lastName,
          middleName: userCredentials.middleName,
          email: userCredentials.email
        })
      });
    } catch (error) {
      reject(error);
    }
  });

  return { validate };
};
