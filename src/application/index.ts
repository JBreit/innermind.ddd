export const application = ({ server, database }) => ({
  start: () => Promise
    .resolve()
    // .then(database.authenticate)
    .then(server.start)
});
